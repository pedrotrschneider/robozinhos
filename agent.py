# MAC0318 Intro to Robotics
# Please fill-in the fields below with every team member info
#
# Name: Bruno Armond Braga
# NUSP: 12542331
#
# Name: Pedro Tonini Rosenberg Schneider
# NUSP: 12543328
#
# Name: Vinicius Henrique Ferraz Lima
# NUSP: 12702541
#
# Any supplemental material for your agent to work (e.g. neural networks, data, etc.) should be
# uploaded elsewhere and listed down below together with a download link.
#
# ---
#
# Final Project - The Travelling Mailduck Problem
#
# Don't forget to run this file from the Duckievillage root directory path (example):
#   cd ~/MAC0318/duckievillage
#   conda activate duckietown
#   python3 assignments/challenge/challenge.py assignments/challenge/examples/challenge_n
#
# Submission instructions:
#  0. Add your names and USP numbers to the file header above.
#  1. Make sure that any last change hasn't broken your code. If the code crashes without running you'll get a 0.
#  2. Submit this file via e-disciplinas.

import pyglet
from pyglet.window import key
import numpy as np
import math
import random
from duckievillage import create_env, FRONT_VIEW_MODE
import cv2
import tensorflow as tf
import sys

class Agent:
    # Agent initialization
    def __init__(self, env):
        """ Initializes agent """
        self.env = env
        self.radius = 0.0318
        self.baseline = env.unwrapped.wheel_dist/2
        self.motor_gain = 0.68*0.0784739898632288
        self.motor_trim = 0.0007500911693361842
        self.initial_pos = env.get_position()

        self.env = env
        # Color segmentation hyperspace - TODO: MODIFY THE VALUES BELOW
        self.inner_lower = np.array([25, 100, 150])
        self.inner_upper = np.array([60, 255, 255])
        self.outer_lower = np.array([0, 0, 160])
        self.outer_upper = np.array([170, 75, 255]) 
        # Acquire image for initializing activation matrices
        img = self.env.front()
        img_shape = img.shape[0], img.shape[1]
        # Filtro para faixa tracejada central
        # amarelo
        self.inner_lower = np.array([15, 80, 160])        
        self.inner_upper = np.array([40, 255, 255])    
        # branco
        self.outer_lower = np.array([0, 0, 165])  
        self.outer_upper = np.array([255, 70, 255])    

        # Acquire image for initializing activation matrices
        self.inner_left_motor_matrix = np.zeros(shape=img_shape, dtype="float32")
        self.inner_right_motor_matrix = np.zeros(shape=img_shape, dtype="float32")
        self.outer_left_motor_matrix = np.zeros(shape=img_shape, dtype="float32")
        self.outer_right_motor_matrix = np.zeros(shape=img_shape, dtype="float32")

        # inner - amarelo
        # outter - branco
        # motor esquerdo
        self.inner_left_motor_matrix[img_shape[0]//2:, :img_shape[1]//2] = 1.3
        self.outer_left_motor_matrix[img_shape[0]//2:, :img_shape[1]//2] = -1
        # motor direito
        self.inner_right_motor_matrix[img_shape[0]//2:, img_shape[1]//2:] = -0.4
        self.outer_right_motor_matrix[img_shape[0]//2:, img_shape[1]//2:] = 1

        # Inicializando modelos de rede neural
        self.max_rede = 50
        self.comeca_braitenberg = 35
        self.usar_rede = 0
        self.modelo_dodge = tf.keras.models.load_model('assignments/challenge/dodge.h5')
        self.modelo_od = tf.keras.models.load_model('assignments/challenge/object_detection.h5')

        key_handler = key.KeyStateHandler()
        env.unwrapped.window.push_handlers(key_handler)
        self.key_handler = key_handler

        self.simulate = True
    
    def get_pwm_control(self, v: float, w: float)-> (float, float):
        ''' Takes velocity v and angle w and returns left and right power to motors.'''
        V_l = (self.motor_gain - self.motor_trim)*(v-w*self.baseline)/self.radius
        V_r = (self.motor_gain + self.motor_trim)*(v+w*self.baseline)/self.radius
        return V_l, V_r

    # Image processing routine - Color segmentation
    def preprocess(self, image: np.ndarray) -> np.ndarray:
        """ Returns a 2D array mask color segmentation of the image """
        hsv = cv2.cvtColor(image, cv2.COLOR_RGB2HSV) # obtain HSV representation of image
        # filter out dashed yellow "inner" line
        inner_mask = cv2.inRange(hsv, self.inner_lower, self.inner_upper)//255
        # filter out solid white "outer" line
        outer_mask = cv2.inRange(hsv, self.outer_lower, self.outer_upper)//255
        # Note: it is possible to filter out pixels in the RGB format
        #  by replacing `hsv` with `image` in the commands above
        # produces combined mask (might or might not be useful)
        mask = cv2.bitwise_or(inner_mask, outer_mask)
        self.masked = cv2.bitwise_and(image, image, mask=mask)
        return inner_mask, outer_mask, mask

    def send_commands(self, dt):
        ''' Agent control loop '''
        # acquire front camera imag
        velocity = 0
        rotation = 0
        img = self.env.front()
        # run image processing routines
        P, Q, M = self.preprocess(img) # returns inner, outter and combined mask matrices
        # build left and right motor signals from connection matrices and masks (this is a suggestion, feel free to modify it)
        L = float(np.sum(P * self.inner_left_motor_matrix)) + float(np.sum(Q * self.outer_left_motor_matrix))
        R = float(np.sum(P * self.inner_right_motor_matrix)) + float(np.sum(Q * self.outer_right_motor_matrix))
        # Upper bound on the values above (very loose bound)
        limit = img.shape[0]*img.shape[1]*2
        # These are big numbers, better to rescale them to the unit interval
        L = rescale(L, 0, limit)
        R = rescale(R, 0, limit)
        # Tweak with the constants below to get to change velocity or to stabilize the behavior
        # Recall that the pwm signal sets the wheel torque, and is capped to be in [-1,1]
        gain = 12   # increasing this will increasing responsitivity and reduce stability
        const = 0.22 # power under null activation - this affects the base velocity
        pwm_left = const + L * gain
        pwm_right = const + R * gain

        img_resized = cv2.resize(img, (80, 60)) / 255.0
        resposta_od = self.modelo_od.predict(np.array([img_resized]), verbose=0).flatten()[0]

        if self.comeca_braitenberg < 0:
            if resposta_od > 0.75:
                if self.usar_rede < 0:
                    self.usar_rede = 0
                if self.usar_rede <= self.max_rede:
                    self.usar_rede += 2
            else:
                self.usar_rede -= 1
            
            if  self.usar_rede > 0:
                resposta_dodge = self.modelo_dodge.predict(np.array([img_resized]), verbose=0).flatten()
                #velocity = resposta_dodge[0] * 1.6
                #rotation = resposta_dodge[1] * 1.9
                velocity = resposta_dodge[0] * 1.6 + 0.025
                rotation = resposta_dodge[1] * 1.90

                pwm_left, pwm_right = self.get_pwm_control(velocity, rotation)


        self.comeca_braitenberg -= 1
        # print(f'> {L:.4f} {R:.4f} {pwm_left:.4f} {pwm_right:.4f}') # uncomment for debugging
        # Now send command to motors
        self.env.step(pwm_left, pwm_right)
        #  for visualization
        self.env.render('human')


def rescale(x: float, L: float, U: float):
    ''' Map scalar x in interval [L, U] to interval [0, 1]. '''
    return (x - L) / (U - L)
